package com.jiten.innovations.springCloud.beans;

public class MessageBean {
	private String message;

	
	
	public MessageBean(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
