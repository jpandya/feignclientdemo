package com.jiten.innovations.springCloud.feignClients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name="User-Service-Micro",url = "localhost:8080") 
public interface UserServiceMicroFeignClient {
	
	@GetMapping("/msg")
	public String getMsg();
		
}
