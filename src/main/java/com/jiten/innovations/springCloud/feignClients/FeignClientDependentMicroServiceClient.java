package com.jiten.innovations.springCloud.feignClients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="FeignClientDependentMicroServiceDemo",url="localhost:9002")
public interface FeignClientDependentMicroServiceClient {
	
	@GetMapping("/msg")
	public String getMsg();
	
}
