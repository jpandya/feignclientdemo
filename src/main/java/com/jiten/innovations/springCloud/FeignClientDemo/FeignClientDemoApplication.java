package com.jiten.innovations.springCloud.FeignClientDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.jiten.innovations.springCloud.controller"})
@EnableFeignClients(basePackages= {"com.jiten.innovations.springCloud.feignClients"})
public class FeignClientDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignClientDemoApplication.class, args);
		System.out.println("DONE");
	}

}
