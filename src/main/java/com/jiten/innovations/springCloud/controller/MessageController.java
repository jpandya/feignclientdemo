package com.jiten.innovations.springCloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jiten.innovations.springCloud.feignClients.FeignClientDependentMicroServiceClient;
import com.jiten.innovations.springCloud.feignClients.UserServiceMicroFeignClient;
import com.jiten.innovations.springCloud.beans.MessageBean;

//@RefreshScope
@RestController
public class MessageController {
	
	@Autowired
	UserServiceMicroFeignClient userServiceMicroFeignClient;
	
	@Autowired
	FeignClientDependentMicroServiceClient feignClientDependentServiceFeignClient;
	
	
	@GetMapping("/open-feign-client/getMessage")
	public MessageBean getMessage() {
		return new MessageBean(feignClientDependentServiceFeignClient.getMsg());
	}
}
